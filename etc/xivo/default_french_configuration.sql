-- IPBX > Trunk management > Customized
INSERT INTO usercustom VALUES (default, 'template_a_changer', NULL, 'local', '@nonexistant', 0, 'custom', 'trunk');

INSERT INTO trunkfeatures VALUES (default, 'custom', 1, 0, 0,
    'Interconnexion configuration par defaut pour la France. A supprimer.',
    (SELECT id FROM mediaserver LIMIT 1));

-- IPBX > Call management > Outgoing calls > Add
INSERT INTO route VALUES (default, NULL, 'sortants-france', 1);
INSERT INTO route VALUES (default, NULL, 'urgences-france', 2);

INSERT INTO routetrunk VALUES (1, 1, 0);
INSERT INTO routetrunk VALUES (2, 1, 0);

-- IPBX > Call management > Outgoing calls > sortants-france
INSERT INTO routepattern VALUES (default, '0[1-5]\d\d\d\d\d\d\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '\+33[1-5]\d\d\d\d\d\d\d\d', '.{3}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '0033[1-5]\d\d\d\d\d\d\d\d', '.{4}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '06\d\d\d\d\d\d\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '\+336\d\d\d\d\d\d\d\d', '.{3}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '00336\d\d\d\d\d\d\d\d', '.{4}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '07[3-9]\d\d\d\d\d\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '\+337[3-9]\d\d\d\d\d\d\d', '.{3}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '00337[3-9]\d\d\d\d\d\d\d', '.{4}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '0700\d\d\d\d\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '\+33700\d\d\d\d\d\d', '.{3}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '0033700\d\d\d\d\d\d', '.{4}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '08\d\d\d\d\d\d\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '\+338\d\d\d\d\d\d\d\d', '.{3}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '00338\d\d\d\d\d\d\d\d', '.{4}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '09\d\d\d\d\d\d\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '\+339\d\d\d\d\d\d\d\d', '.{3}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '00339\d\d\d\d\d\d\d\d', '.{4}(.*)', '0\1', NULL, 1);
INSERT INTO routepattern VALUES (default, '3\d\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '116\d\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '118\d\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '10\d\d', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '00[1-9]\d[0-9#\*]+', NULL, NULL, NULL, 1);
INSERT INTO routepattern VALUES (default, '\+[1-9]\d[0-9#\*]+', '.{1}(.*)', '00\1', NULL, 1);

-- IPBX > Call management > Outgoing calls > urgences-france
INSERT INTO routepattern VALUES (default, '15', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '17', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '18', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '112', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '114', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '115', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '116000', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '119', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '191', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '196', NULL, NULL, NULL, 2);
INSERT INTO routepattern VALUES (default, '197', NULL, NULL, NULL, 2);

-- IPBX > Call management > Call permissions
INSERT INTO rightcall VALUES (default, 'national', '', 1, 0, '');
INSERT INTO rightcall VALUES (default, 'urgences', '', 1, 0, '');
INSERT INTO rightcall VALUES (default, 'mobiles', '', 1, 0, '');
INSERT INTO rightcall VALUES (default, 'numeros-a-valeur-ajoutee', '', 1, 0, '');
INSERT INTO rightcall VALUES (default, 'international', '', 1, 0, '');
INSERT INTO rightcall VALUES (default, 'refuser-tout', '', 0, 0, '');

-- IPBX > Call management > Call permissions > General
INSERT INTO rightcallexten VALUES (default, 1, '_0[1-5]XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 1, '_+33[1-5]XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 1, '_0033[1-5]XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 1, '_+339XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 1, '_00339XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 1, '_09XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 2, '15');
INSERT INTO rightcallexten VALUES (default, 2, '17');
INSERT INTO rightcallexten VALUES (default, 2, '18');
INSERT INTO rightcallexten VALUES (default, 2, '112');
INSERT INTO rightcallexten VALUES (default, 2, '114');
INSERT INTO rightcallexten VALUES (default, 2, '115');
INSERT INTO rightcallexten VALUES (default, 2, '116000');
INSERT INTO rightcallexten VALUES (default, 2, '119');
INSERT INTO rightcallexten VALUES (default, 2, '191');
INSERT INTO rightcallexten VALUES (default, 2, '196');
INSERT INTO rightcallexten VALUES (default, 2, '197');
INSERT INTO rightcallexten VALUES (default, 2, '_116XXX');
INSERT INTO rightcallexten VALUES (default, 3, '_06XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 3, '_+336XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 3, '_00336XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 3, '_07[3-9]XXXXXXX');
INSERT INTO rightcallexten VALUES (default, 3, '_+337[3-9]XXXXXXX');
INSERT INTO rightcallexten VALUES (default, 3, '_00337[3-9]XXXXXXX');
INSERT INTO rightcallexten VALUES (default, 3, '_0700XXXXXX');
INSERT INTO rightcallexten VALUES (default, 3, '_+33700XXXXXX');
INSERT INTO rightcallexten VALUES (default, 3, '_0033700XXXXXX');
INSERT INTO rightcallexten VALUES (default, 4, '_08XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 4, '_+338XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 4, '_00338XXXXXXXX');
INSERT INTO rightcallexten VALUES (default, 4, '_3XXX');
INSERT INTO rightcallexten VALUES (default, 4, '_118XXX');
INSERT INTO rightcallexten VALUES (default, 4, '_10XX');
INSERT INTO rightcallexten VALUES (default, 5, '_00ZX.');
INSERT INTO rightcallexten VALUES (default, 5, '_+ZX.');
INSERT INTO rightcallexten VALUES (default, 6, '_X.');
INSERT INTO rightcallexten VALUES (default, 6, '_+ZX.');
