# XiVO config

Debian package for XiVO PBX and XiVO MDS featuring various configuration scripts and templates.

## Running unit tests

Create a virtual environment

```python
python3 -m venv config-venv
source config-venv/bin/activate
```

Install the test-requirements then run the tests

```python
pip install -r test-requirements.txt
tox
```

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not,
see <http://www.gnu.org/licenses/>.

See the COPYING file for details.
